
/* Drop Indexes */

DROP INDEX IF EXISTS ATT_VIRUSTOTAL_RESULT_BINARY_ID_IDX;



/* Drop Tables */

DROP TABLE IF EXISTS TS_MAIN_MIN;
DROP TABLE IF EXISTS AGG_MAIN;
DROP TABLE IF EXISTS ATT_ASN_SHADOWSERVER;
DROP TABLE IF EXISTS ATT_GEOIP_SHADOWSERVER;
DROP TABLE IF EXISTS ATT_VIRUSTOTAL_RESULT;
DROP TABLE IF EXISTS ATT_VIRUSTOTAL_REPORT;
DROP TABLE IF EXISTS DIM_BINARY;
DROP TABLE IF EXISTS DIM_IDENT;
DROP TABLE IF EXISTS DIM_SOURCE_IP;
DROP TABLE IF EXISTS DIM_TARGET_IP;
DROP TABLE IF EXISTS DIM_TARGET_PORT;
DROP TABLE IF EXISTS X_LOG_MAIN;




/* Create Tables */

CREATE TABLE AGG_MAIN
(
	AGG_ID BIGSERIAL NOT NULL UNIQUE,
	IDENT_ID BIGINT NOT NULL,
	BINARY_ID BIGINT NOT NULL,
	SOURCE_IP INET NOT NULL,
	TARGET_PORT INT NOT NULL,
	TARGET_IP INET NOT NULL,
	N_COUNT BIGINT,
	TS_LAST TIMESTAMP WITH TIME ZONE,
	TS_FIRST TIMESTAMP WITH TIME ZONE,
	PRIMARY KEY (AGG_ID),
	CONSTRAINT agg_main_key UNIQUE (IDENT_ID, BINARY_ID, SOURCE_IP, TARGET_PORT, TARGET_IP)
) WITHOUT OIDS;


CREATE TABLE ATT_ASN_SHADOWSERVER
(
	IP INET NOT NULL UNIQUE,
	ASN BIGINT,
	AS_NAME VARCHAR,
	CC CHAR(2),
	DOM VARCHAR,
	ISP VARCHAR,
	BGP_PREFIX INET,
	TS TIMESTAMP WITH TIME ZONE,
	PRIMARY KEY (IP)
) WITHOUT OIDS;


CREATE TABLE ATT_GEOIP_SHADOWSERVER
(
	IP INET NOT NULL UNIQUE,
	CC CHAR(2),
	CITY VARCHAR,
	LATITUDE FLOAT,
	LONGITUDE FLOAT,
	TS TIMESTAMP WITH TIME ZONE,
	PRIMARY KEY (IP)
) WITHOUT OIDS;


CREATE TABLE ATT_VIRUSTOTAL_REPORT
(
	BINARY_ID BIGINT NOT NULL UNIQUE,
	RESPONSE_CODE INT,
	RESULT_COUNT BIGINT,
	SCAN_ID VARCHAR,
	SCAN_DATE TIMESTAMP WITH TIME ZONE,
	PERMALINK VARCHAR,
	TS TIMESTAMP WITH TIME ZONE,
	PRIMARY KEY (BINARY_ID)
) WITHOUT OIDS;


CREATE TABLE ATT_VIRUSTOTAL_RESULT
(
	BINARY_ID BIGINT NOT NULL,
	LABEL VARCHAR,
	ENGINE VARCHAR,
	E_VERSION VARCHAR,
	E_UPDATE VARCHAR
) WITHOUT OIDS;


CREATE TABLE DIM_BINARY
(
	ID BIGSERIAL NOT NULL UNIQUE,
	MD5 CHAR(32) NOT NULL UNIQUE,
	SHA512 CHAR(128),
	N_COUNT BIGINT,
	TS_LAST TIMESTAMP WITH TIME ZONE,
	TS_FIRST TIMESTAMP WITH TIME ZONE,
	PRIMARY KEY (ID)
) WITHOUT OIDS;


CREATE TABLE DIM_IDENT
(
	ID BIGSERIAL NOT NULL UNIQUE,
	IDENT VARCHAR(16) NOT NULL UNIQUE,
	N_COUNT BIGINT,
	TS_LAST TIMESTAMP WITH TIME ZONE,
	TS_FIRST TIMESTAMP WITH TIME ZONE,
	PRIMARY KEY (ID)
) WITHOUT OIDS;


CREATE TABLE DIM_SOURCE_IP
(
	IP INET NOT NULL UNIQUE,
	N_COUNT BIGINT,
	TS_LAST TIMESTAMP WITH TIME ZONE,
	TS_FIRST TIMESTAMP WITH TIME ZONE,
	PRIMARY KEY (IP)
) WITHOUT OIDS;


CREATE TABLE DIM_TARGET_IP
(
	IP INET NOT NULL UNIQUE,
	N_COUNT BIGINT,
	TS_LAST TIMESTAMP WITH TIME ZONE,
	TS_FIRST TIMESTAMP WITH TIME ZONE,
	PRIMARY KEY (IP)
) WITHOUT OIDS;


CREATE TABLE DIM_TARGET_PORT
(
	PORT INT NOT NULL UNIQUE,
	N_COUNT BIGINT,
	TS_LAST TIMESTAMP WITH TIME ZONE,
	TS_FIRST TIMESTAMP WITH TIME ZONE,
	PRIMARY KEY (PORT)
) WITHOUT OIDS;


CREATE TABLE TS_MAIN_MIN
(
	TS_MIN TIMESTAMP WITH TIME ZONE NOT NULL,
	AGG_ID BIGINT NOT NULL,
	N_COUNT BIGINT,
	CONSTRAINT ts_main_min_key UNIQUE (TS_MIN, AGG_ID)
) WITHOUT OIDS;


CREATE TABLE X_LOG_MAIN
(
	ID BIGSERIAL NOT NULL UNIQUE,
	LAST_ID BIGINT,
	FIRST_ID BIGINT,
	N_ROWS BIGINT,
	TS_COMPL TIMESTAMP WITH TIME ZONE,
	TS_START TIMESTAMP WITH TIME ZONE,
	PRIMARY KEY (ID)
) WITHOUT OIDS;



/* Create Foreign Keys */

ALTER TABLE TS_MAIN_MIN
	ADD FOREIGN KEY (AGG_ID)
	REFERENCES AGG_MAIN (AGG_ID)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;


ALTER TABLE ATT_VIRUSTOTAL_RESULT
	ADD FOREIGN KEY (BINARY_ID)
	REFERENCES ATT_VIRUSTOTAL_REPORT (BINARY_ID)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;


ALTER TABLE AGG_MAIN
	ADD FOREIGN KEY (BINARY_ID)
	REFERENCES DIM_BINARY (ID)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;


ALTER TABLE ATT_VIRUSTOTAL_REPORT
	ADD FOREIGN KEY (BINARY_ID)
	REFERENCES DIM_BINARY (ID)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;


ALTER TABLE AGG_MAIN
	ADD FOREIGN KEY (IDENT_ID)
	REFERENCES DIM_IDENT (ID)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;


ALTER TABLE AGG_MAIN
	ADD FOREIGN KEY (SOURCE_IP)
	REFERENCES DIM_SOURCE_IP (IP)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;


ALTER TABLE AGG_MAIN
	ADD FOREIGN KEY (TARGET_IP)
	REFERENCES DIM_TARGET_IP (IP)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;


ALTER TABLE AGG_MAIN
	ADD FOREIGN KEY (TARGET_PORT)
	REFERENCES DIM_TARGET_PORT (PORT)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;



/* Create Indexes */

CREATE INDEX ATT_VIRUSTOTAL_RESULT_BINARY_ID_IDX ON ATT_VIRUSTOTAL_RESULT (BINARY_ID);



