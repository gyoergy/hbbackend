package org.honeynet.hbbackend.virustotal;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Iterator;
import java.util.Locale;
import java.util.TimeZone;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;
import javax.ejb.EJBException;
import javax.ejb.SessionContext;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.ejb.Timeout;
import javax.ejb.Timer;
import javax.ejb.TimerConfig;
import javax.ejb.TimerService;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;
import javax.jms.Topic;
import javax.jms.TopicSubscriber;
import javax.sql.DataSource;

import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


@Singleton
@Startup
public class VirusTotal {
	public static final String SUB_NAME = "virustotal";
	private static final int PAUSE = 15000;
	private static final int RECV_TIMEOUT = 1000;
	
	private static final String API_URL = "https://www.virustotal.com/vtapi/v2/file/report";
	
	private static Logger log = LoggerFactory.getLogger(VirusTotal.class);
	
	
	@Resource(mappedName="jdbc/hbbackend")
	private DataSource ds;
	
	@Resource(mappedName="jms/DurableConsumer/virustotal")
	private ConnectionFactory jmsConnectionFactory;
	
	@Resource(mappedName="jms/new_binary")
	private Topic jms_new_binary;
	
	@Resource
	SessionContext ctx;
	
	@Resource
	TimerService timerService;
	
	
	@Resource(name="apiKey")
	private String apiKey;
	
	
	private boolean stop = false;
	private String emsg;
	
	
	@PostConstruct
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	private void start() {
		log.trace("start()");
		TimerConfig tc = new TimerConfig();
		tc.setPersistent(false);
		Timer t = timerService.createSingleActionTimer(1000, tc);
	}
	
	
	@Timeout
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void work() {
		log.trace("work()");
		if (stop) return;
		
		javax.jms.Connection mq = null;
		Session sess = null;
		TopicSubscriber sub = null;
		
		try {
			mq = jmsConnectionFactory.createConnection();
			sess = mq.createSession(true, 0);
			sub = sess.createDurableSubscriber(jms_new_binary, SUB_NAME);
			mq.start();
			
			Message msg;
			do {
				if (stop) return;
				log.trace("sub.receive()");
				msg = sub.receive(RECV_TIMEOUT);
			}
			while (msg == null);
						
			if (!stop) {
				long binaryId = Long.parseLong(msg.getStringProperty("id"));
				String md5 = msg.getStringProperty("md5");
				log.debug("received new msg | id = {} md5 = {}", binaryId, md5);
				
				store(binaryId, retrieve(md5));
			}
			else {
				ctx.setRollbackOnly();
				return;
			}
			
			if (!stop) {
				TimerConfig tc = new TimerConfig();
				tc.setPersistent(false);
				Timer t = timerService.createSingleActionTimer(PAUSE, tc);
				log.trace("work() timer created");
			}
		}
		catch (JMSException e) {
			log.error("got JMSException", e);
			throw new EJBException(e);
		}
		finally {
			try {
				if (sub != null) sub.close(); 
				if (sess != null) sess.close();
				if (mq != null)	mq.close();
			} catch (JMSException e) {
				log.error("got JMSException while closing resource", e);
			}
		}
	}
	
	
	private JsonNode retrieve(String md5) {
		log.trace("retrieve()");
		
		String req =
			"apikey=" + apiKey + "&" +
			"resource=" + md5;
		
		OutputStreamWriter out = null;
        InputStream in = null;
        JsonNode report;
        
        try {
            URL url = new URL(API_URL);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();

            conn.setDoOutput(true);
            
            log.debug("sending request: " + req);
            out = new OutputStreamWriter(conn.getOutputStream());
            out.write(req);
            out.flush();
            
            int res = conn.getResponseCode();
            if (res == 403) {
            	emsg = "got http 403 - invalid api key: " + apiKey;
            	log.error(emsg);
            	throw new EJBException(emsg);
            }
            if (res == 204) {
            	emsg = "got http 204 - request rate limit exceeded";
            	log.error(emsg);
            	throw new EJBException(emsg);
            }
            
            in = conn.getInputStream();
            ObjectMapper mapper = new ObjectMapper();
            report = mapper.readValue(in, JsonNode.class);
            
            log.debug("read response: response_code: {}", report.path("response_code").getValueAsText());
            return report;
            
	    }
        catch (MalformedURLException e) {
	            log.error("got MalformedURLException", e);
	            throw new EJBException(e);
	    }
        catch (IOException e) {
	            log.error("got IOException", e);
	            throw new EJBException(e);
	    }
        finally {
			try {
				if (out != null) out.close();
				if (in != null) in.close();
			}
			catch (IOException e) {
				log.error("got IOException", e);
				throw new EJBException(e);
			}
	    }
	}
	
	
	private void store(long binaryId, JsonNode report) {
		log.trace("store()");
		
		JsonNode responseCodeField = report.get("response_code");
		JsonNode scansObject = report.get("scans");
		JsonNode scanIdField = report.get("scan_id");
		JsonNode scanDateField = report.get("scan_date");
		JsonNode permalinkField = report.get("permalink");
		
		String scanId;
		Timestamp scanDate;
		String permalink;
		
		// response_code
		if (responseCodeField == null) {
			emsg = "invalid response: missing response_code field";
			log.error(emsg);
			throw new EJBException(emsg);
		}
		if (!responseCodeField.isNumber()) {
			emsg = "invalid field: response_code: " + responseCodeField.getValueAsText();
			log.error(emsg);
			throw new EJBException(emsg);
		}
		
		
		// insert empty record here and return
		if (responseCodeField.getIntValue() != 1) {
			Connection db = null;
			try {
				db = ds.getConnection();
				
				PreparedStatement pStmt;
				ResultSet queryRes;
				
				long reportId;
				
				pStmt = db.prepareStatement("insert into virustotal_reports (binary_id, response_code) values (?,?) returning id");
				pStmt.setLong(1, binaryId);
				pStmt.setInt(2, responseCodeField.getIntValue());
				queryRes = pStmt.executeQuery();
				queryRes.next();
				reportId = queryRes.getLong(1);
				queryRes.close();
				pStmt.close();
				log.debug("inserted into virustotal_reports | binary_id={} response_code={} id={}", new Object[] { binaryId, responseCodeField.getIntValue(), reportId });
				
				return;
			}
			catch (SQLException e) {
				log.error("got SQLException", e);
				throw new EJBException(e);
			}
			finally {
				try {
					if (db != null)	db.close();
				}
				catch (SQLException e) {
					log.error("got SQLException while closing resource", e);
				}
			}	
		}
		
		
		// scans
		if (scansObject == null) {
			emsg = "invalid response: missing scans field";
			log.error(emsg);
			throw new EJBException(emsg);
		}
		if (!scansObject.isObject()) {
			emsg = "invalid field: scans: " + scansObject.getValueAsText();
			log.error(emsg);
			throw new EJBException(emsg);
		}
		
		// scan_id
		if (scanIdField == null) {
			emsg = "invalid response: missing scan_id field";
			log.error(emsg);
			throw new EJBException(emsg);
		}
		scanId = scanIdField.getValueAsText();
		
		// scan_date
		if (scanDateField == null) {
			emsg = "invalid response: missing scan_date field";
			log.error(emsg);
			throw new EJBException(emsg);
		}
		try {
			DateFormat dateFormat =
				new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ROOT);
			dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
			scanDate = new Timestamp(dateFormat.parse(scanDateField.getValueAsText()).getTime());
		}
		catch (ParseException e) {
			emsg = "invalid field: scan_date: " + scanDateField.getValueAsText();
			log.error(emsg);
			throw new EJBException(emsg, e);
		}
		
		// permalink
		if (permalinkField == null) {
			emsg = "invalid response: missing permalink field";
			log.error(emsg);
			throw new EJBException(emsg);
		}
		permalink = permalinkField.getValueAsText();
		
		log.trace("field: scan_id: {}", scanId);
		log.trace("field: scan_date: {}", scanDate);
		log.trace("field: permalink: {}", permalink);
		
		// insert
		Connection db = null;
		try {
			db = ds.getConnection();
			
			PreparedStatement pStmt;
			ResultSet queryRes;
			
			long reportId;
			
			pStmt = db.prepareStatement("insert into virustotal_reports (binary_id, response_code, scan_id, scan_date, permalink) values (?,?,?,?,?) returning id");
			pStmt.setLong(1, binaryId);
			pStmt.setInt(2, responseCodeField.getIntValue());
			pStmt.setString(3, scanId);
			pStmt.setTimestamp(4, scanDate);
			pStmt.setString(5, permalink);
			queryRes = pStmt.executeQuery();
			queryRes.next();
			reportId = queryRes.getLong(1);
			queryRes.close();
			pStmt.close();
			log.debug("inserted into virustotal_reports | binary_id={} response_code={} id={}", new Object[]{ binaryId, responseCodeField.getIntValue(), reportId });
			
			// insert results
			int n = 0;
			for (Iterator<String> i = scansObject.getFieldNames(); i.hasNext();) {
				String engine = i.next();
				JsonNode resultObject = scansObject.get(engine);
				
				boolean detected;
				
				try {
					detected = resultObject.get("detected").getValueAsBoolean();
				}
				catch (NullPointerException e) {
					emsg = "invalid response: missing detected field in scans";
					log.error(emsg);
					throw new EJBException(emsg);
				}
				
				// insert only detected
				if (detected) {
					n++;
					String result, version, update;
					
					// check result
					try {
						result = resultObject.get("result").getValueAsText();
					}
					catch (NullPointerException e) {
						emsg = "invalid response: missing result field in scans";
						log.error(emsg);
						throw new EJBException(emsg);
					}
					// check version
					try {
						version = resultObject.get("version").getValueAsText();
					}
					catch (NullPointerException e) {
						emsg = "invalid response: missing version field in scans";
						log.error(emsg);
						throw new EJBException(emsg);
					}
					// check update
					try {
						update = resultObject.get("update").getValueAsText();
					}
					catch (NullPointerException e) {
						emsg = "invalid response: missing update field in scans";
						log.error(emsg);
						throw new EJBException(emsg);
					}
					
					log.trace("result:");
					log.trace("| result: {}", result);
					log.trace("| engine: {}", engine);
					log.trace("| version: {}", version);
					log.trace("| update: {}", update);
					
					// insert
					pStmt = db.prepareStatement("insert into virustotal_results (report_id, label, engine, e_version, e_update) values (?,?,?,?,?)");
					pStmt.setLong(1, reportId);
					pStmt.setString(2, result);
					pStmt.setString(3, engine);
					pStmt.setString(4, version);
					pStmt.setString(5, update);
					pStmt.executeUpdate();
					pStmt.close();
				}
				else {
					continue;
				}
			}
						
			// update results count
			pStmt = db.prepareStatement("update virustotal_reports set result_count = ? where id = ?");
			pStmt.setLong(1, n);
			pStmt.setLong(2, reportId);
			pStmt.executeUpdate();
			pStmt.close();
			
			log.debug("inserted {} results into virustotal_results", n);
		}
		
		catch (SQLException e) {
			log.error("got JMSException", e);
			throw new EJBException(e);
		}
		finally {
			try {
				if (db != null)	db.close();
			}
			catch (SQLException e) {
				log.error("got SQLException while closing resource", e);
			}
		}
	}
	
	
	@PreDestroy
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	private void stop() {
		log.trace("stop()");
		stop = true;
    }
}