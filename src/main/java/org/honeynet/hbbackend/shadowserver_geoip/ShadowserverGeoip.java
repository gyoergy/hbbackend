package org.honeynet.hbbackend.shadowserver_geoip;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.regex.Pattern;

import javax.annotation.Resource;
import javax.ejb.ApplicationException;
import javax.ejb.EJBException;
import javax.ejb.Schedule;
import javax.ejb.SessionContext;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;
import javax.jms.Topic;
import javax.jms.TopicSubscriber;
import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


@Singleton
@Startup
public class ShadowserverGeoip {
	private final static String SERVER = "asn.shadowserver.org";
	private final static int SERVER_PORT = 45;
	private final static int SO_TIMEOUT = 3000;
    private final static Pattern SPLIT_REGEX = Pattern.compile("\\s\\|\\s");
    
    public static final String SUB_NAME = "shadowserver_geoip";
	
	private static Logger log = LoggerFactory.getLogger(ShadowserverGeoip.class);
	
	
	@Resource(mappedName="jdbc/hbbackend")
	private DataSource ds;
	
	@Resource(mappedName="jms/DurableConsumer/shadowserver_geoip")
	private ConnectionFactory jmsConnectionFactory;
	
	@Resource(mappedName="jms/new_ip")
	private Topic jms_new_ip;
	
	@Resource
	SessionContext ctx;
	
	private String emsg;
	

	@ApplicationException(rollback=true)
	public static class EmptyResponseException extends Exception { }
	
	
	@Schedule(second="*/1", minute="*", hour="*", persistent=false)
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void work() throws EmptyResponseException {
		log.trace("work()");
		
		javax.jms.Connection mq = null;
		Session sess = null;
		TopicSubscriber sub = null;

		try {
			mq = jmsConnectionFactory.createConnection();
			sess = mq.createSession(true, 0);
			sub = sess.createDurableSubscriber(jms_new_ip, SUB_NAME);
			mq.start();
			
			Message msg = sub.receiveNoWait();
			if (msg == null) return;
			
			String ip = msg.getStringProperty("ip");
			log.debug("received new msg | ip={}", ip);
			try {
				store(ip, retrieve(ip));
			}
			catch (EmptyResponseException e) {
				if (msg.getJMSRedelivered()) {
					log.warn("ignoring due to empty response | ip={}", ip);
					return;
				} else {
					throw e;
				}
			}
		}
		catch (JMSException e) {
			log.error("got JMSException", e);
			throw new EJBException(e);
		}
		finally {
			try {
				if (sub != null) sub.close(); 
				if (sess != null) sess.close();
				if (mq != null)	mq.close();
			} catch (JMSException e) {
				log.error("got JMSException while closing resource", e);
			}
		}
	}
	
	
	private Result retrieve(String ip) throws EmptyResponseException {
		log.trace("retrieve()");
		
		try {
			log.debug("querying {}:{} | ip={}", new Object[]{ SERVER, SERVER_PORT, ip });
			Socket socket = new Socket(SERVER, SERVER_PORT);
			socket.setSoTimeout(SO_TIMEOUT);
			
			BufferedReader in
				= new BufferedReader(new InputStreamReader(socket.getInputStream(), "UTF-8"));
			PrintWriter out
				= new PrintWriter(socket.getOutputStream());
			
			// request
			out.println("begin geo");
			out.println(ip);
			out.println("end");
			out.flush();
			
			// response
			String response = in.readLine();
			log.trace("response: {}", response);
			
			out.close();
			in.close();
			socket.close();
			
			if (response == null) {
				log.warn("empty response | ip={}", ip);
				throw new EmptyResponseException();
			}
			
			// format: (0) ip | (1) country code | (2) (empty) | (3) (ignored) | (4) city | (5) latitude | (6) longitude
			String[] responseFields = SPLIT_REGEX.split(response);
			if (responseFields.length != 7) {
				emsg = "invalid number of fields in response";
				log.error(emsg);
				throw new EJBException(emsg);
			}
			 
			Result result = new Result();
			int i = -1;
			try {
				result.cc = handleStringField(responseFields[i=1]);
				result.city = handleStringField(responseFields[i=4], true);
				result.latitude = handleFloatField(responseFields[i=5], true);
				result.longitude = handleFloatField(responseFields[i=6], true);
			}
			catch (ValidationException e) {
				emsg = "invalid response: field " + i + ": " + e.getMessage();
				log.error(emsg);
				throw new EJBException(emsg);
			}
			
			log.trace("parsed response: {}", result);
			return result;
		}
		catch (UnknownHostException e) {
			log.error("got UnknownHostException", e);
			throw new EJBException(e);
			
		}
		catch (SocketTimeoutException e) {
			log.error("got SocketTimeoutException", e);
			throw new EJBException(e);
		}
		catch (IOException e) {
			log.error("got IOException", e);
			throw new EJBException(e);
		}
	}
	
	
	private void store(String ip, Result result) {
		log.trace("retrieve()");
		
		Connection db = null;
		try {
			db = ds.getConnection();
			
			PreparedStatement pStmt;
			ResultSet queryRes;
			
			long recordId;
			
			pStmt = db.prepareStatement("insert into geoip_shadowserver (ip, cc, city, latitude, longitude) values (inet(?),?,?,?,?) returning id");
			pStmt.setString(1, ip);
			pStmt.setString(2, result.cc);
			
			if (result.city == null)
				pStmt.setNull(3, Types.VARCHAR);
			else pStmt.setString(3, result.city);
			
			if (result.latitude == null)
				pStmt.setNull(4, Types.FLOAT);
			else pStmt.setFloat(4, result.latitude);
			
			if (result.longitude == null)
				pStmt.setNull(5, Types.FLOAT);
			else pStmt.setFloat(5, result.longitude);
			
			queryRes = pStmt.executeQuery();
			queryRes.next();
			recordId = queryRes.getLong(1);
			queryRes.close();
			pStmt.close();
			log.debug("inserted into geoip_shadowserver | ip={} id={}", ip, recordId);
		}
		catch (SQLException e) {
				log.error("got SQLException", e);
				throw new EJBException(e);
		}
		finally {
			try {
				if (db != null)	db.close();
			}
			catch (SQLException e) {
				log.error("got SQLException while closing resource", e);
			}
		}
	}
	
	
	private static String handleStringField(String value, boolean allowEmpty) throws ValidationException {
		value = value.trim();
		if (value.equals("")) {
			if (allowEmpty) {
				return null;
			}
			else {
				throw new ValidationException("empty field");
			}
		}
        return value;
	}
	
	private static String handleStringField(String value) throws ValidationException {
		return handleStringField(value, false);
	}
	
	private static Float handleFloatField(String value, boolean allowEmpty) throws ValidationException {
		value = value.trim();
		if (value.equals("")) {
			if (allowEmpty) {
				return null;
			}
			else {
				throw new ValidationException("number not parsable");
			}
		}
    	try {
    		return Float.parseFloat(value);
    	}
    	catch (NumberFormatException e) {
    		throw new ValidationException("number not parsable");
		}
	}
	
	private static Float handleFloatField(String value) throws ValidationException {
		return handleFloatField(value, false);
	}
	
	
	private static class Result {	
		String cc;
		String city;
		Float latitude;
		Float longitude;
		
		@Override
		public String toString() {
			return String.format(
					"cc = %s | city = %s | latitude = %f | longitude = %f",
					cc, city, latitude, longitude);
		}
	}
	
	private static class ValidationException extends Exception { 
		public ValidationException(String msg) {
			super(msg);
		}
	}
}
