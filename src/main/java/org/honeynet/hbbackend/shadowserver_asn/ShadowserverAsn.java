package org.honeynet.hbbackend.shadowserver_asn;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.regex.Pattern;

import javax.annotation.Resource;
import javax.ejb.EJBException;
import javax.ejb.Schedule;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;
import javax.jms.Topic;
import javax.jms.TopicSubscriber;
import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


@Singleton
@Startup
public class ShadowserverAsn {
	private final static String SERVER = "asn.shadowserver.org";
	private final static int SERVER_PORT = 43;
	private final static int SO_TIMEOUT = 3000;
    private final static Pattern SPLIT_REGEX = Pattern.compile("\\s\\|\\s");
    
    public static final String SUB_NAME = "shadowserver_asn";
	
	private static Logger log = LoggerFactory.getLogger(ShadowserverAsn.class);
	
	
	@Resource(mappedName="jdbc/hbbackend")
	private DataSource ds;
	
	@Resource(mappedName="jms/DurableConsumer/shadowserver_asn")
	private ConnectionFactory jmsConnectionFactory;
	
	@Resource(mappedName="jms/new_ip")
	private Topic jms_new_ip;
	
	
	private String emsg;
	
	
	public static class IncompleteResponseException extends Exception { }
	
	
	@Schedule(second="*/1", minute="*", hour="*", persistent=false)
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void work() {
		log.trace("work()");
		
		javax.jms.Connection mq = null;
		Session sess = null;
		TopicSubscriber sub = null;

		try {
			mq = jmsConnectionFactory.createConnection();
			sess = mq.createSession(true, 0);
			sub = sess.createDurableSubscriber(jms_new_ip, SUB_NAME);
			mq.start();
			
			Message msg = sub.receiveNoWait();
			if (msg == null) return;
			
			String ip = msg.getStringProperty("ip");
			log.debug("received new msg | ip = {}", ip);
			
			try {
				store(ip, retrieve(ip));
			}
			catch (IncompleteResponseException e) {
				return;
			}
		}
		catch (JMSException e) {
			log.error("got JMSException", e);
			throw new EJBException(e);
		}
		finally {
			try {
				if (sub != null) sub.close(); 
				if (sess != null) sess.close();
				if (mq != null)	mq.close();
			} catch (JMSException e) {
				log.error("got JMSException while closing resource", e);
			}
		}
	}
	
	
	private Result retrieve(String ip) throws IncompleteResponseException {
		log.trace("retrieve()");
		
		try {
			log.debug("querying {}:{} | ip={}", new Object[]{ SERVER, SERVER_PORT, ip });
			Socket socket = new Socket(SERVER, SERVER_PORT);
			socket.setSoTimeout(SO_TIMEOUT);
			
			BufferedReader in
				= new BufferedReader(new InputStreamReader(socket.getInputStream(), "UTF-8"));
			PrintWriter out
				= new PrintWriter(socket.getOutputStream());
			
			// request
			out.println("begin origin");
			out.println(ip);
			out.println("end");
			out.flush();
			
			// response
			String response = in.readLine();
			log.trace("response: {}", response);
			
			out.close();
			in.close();
			socket.close();
			
			if (response == null) {
				emsg = "no response";
				log.error(emsg);
				throw new EJBException(emsg);
			}
			
			// format: (0) IP | (1) ASN | (2) Prefix | (3) AS Name | (4) CN | (5) Domain | (6) ISP
			String[] responseFields = SPLIT_REGEX.split(response);
			if (responseFields.length != 7) {
				emsg = "invalid number of fields in response";
				log.error(emsg);
				throw new EJBException(emsg);
			}
			 
			Result result = new Result();
			int i = -1;
			try {
				result.asn = handleLongField(responseFields[i=1], true);
				result.as_name = handleStringField(responseFields[i=3], true);
				result.cc = handleStringField(responseFields[i=4], true);
				result.dom = handleStringField(responseFields[i=5], true);
				result.isp = handleStringField(responseFields[i=6], true);
				result.bgp_prefix = handleStringField(responseFields[i=2], true);
				
				if (result.asn == null && result.cc == null) {
					log.warn("ignoring response as neither asn nor cc is specified | ip={}");
					throw new IncompleteResponseException();
				}
			}
			catch (ValidationException e) {
				emsg = "invalid response: field " + i + ": " + e.getMessage();
				log.error(emsg);
				throw new EJBException(emsg);
			}
			
			log.trace("parsed response: {}", result);
			return result;
		}
		catch (UnknownHostException e) {
			log.error("got UnknownHostException", e);
			throw new EJBException(e);
			
		}
		catch (SocketTimeoutException e) {
			log.error("got SocketTimeoutException", e);
			throw new EJBException(e);
		}
		catch (IOException e) {
			log.error("got IOException", e);
			throw new EJBException(e);
		}
	}
	
	
	private void store(String ip, Result result) {
		log.trace("retrieve()");
		
		Connection db = null;
		try {
			db = ds.getConnection();
			
			PreparedStatement pStmt;
			ResultSet queryRes;
			
			long recordId;
			
			pStmt = db.prepareStatement("insert into asn_shadowserver (ip, asn, as_name, cc, dom, isp, bgp_prefix) values (inet(?),?,?,?,?,?,inet(?)) returning id");
			pStmt.setString(1, ip);
			if (result.asn == null)
				pStmt.setNull(2, Types.BIGINT);
			else pStmt.setLong(2, result.asn);
			if (result.as_name == null)
				pStmt.setNull(3, Types.VARCHAR);
			else pStmt.setString(3, result.as_name);
			if (result.cc == null)
				pStmt.setNull(4, Types.VARCHAR);
			else pStmt.setString(4, result.cc);
			if (result.dom == null)
				pStmt.setNull(5, Types.VARCHAR);
			else pStmt.setString(5, result.dom);
			if (result.isp == null)
				pStmt.setNull(6, Types.VARCHAR);
			else pStmt.setString(6, result.isp);
			if (result.bgp_prefix == null)
				pStmt.setNull(7, Types.VARCHAR);
			else pStmt.setString(7, result.bgp_prefix);
			queryRes = pStmt.executeQuery();
			queryRes.next();
			recordId = queryRes.getLong(1);
			queryRes.close();
			pStmt.close();
			log.debug("inserted into asn_shadowserver | ip={} id={}", ip, recordId);
		}
		catch (SQLException e) {
				log.error("got SQLException", e);
				throw new EJBException(e);
		}
		finally {
			try {
				if (db != null)	db.close();
			}
			catch (SQLException e) {
				log.error("got SQLException while closing resource", e);
			}
		}
	}
	
	
	private static String handleStringField(String value, boolean allowEmpty) throws ValidationException {
		value = value.trim();
		if (value.equals("")) {
			if (allowEmpty) {
				return null;
			}
			else {
				throw new ValidationException("empty field");
			}
		}
        return value;
	}
	
	private static String handleStringField(String value) throws ValidationException {
		return handleStringField(value, false);
	}
	
	private static Long handleLongField(String value, boolean allowEmpty) throws ValidationException {
		value = value.trim();
		if (value.equals("")) {
			if (allowEmpty) {
				return null;
			}
			else {
				throw new ValidationException("number not parsable");
			}
		}
    	try {
    		return Long.parseLong(value);
    	}
    	catch (NumberFormatException e) {
    		throw new ValidationException("number not parsable");
		}
	}
	
	private static Long handleLongField(String value) throws ValidationException {
		return handleLongField(value, false);
	}
	
	
	private static class Result {	
		Long asn;
		String as_name;
		String cc;
		String dom;
		String isp;
		String bgp_prefix;
		
		@Override
		public String toString() {
			return String.format(
					"asn = %d | as_name = %s | cc = %s | dom = %s | isp = %s | bgp_prefix = %s",
					asn, as_name, cc, dom, isp, bgp_prefix);
		}
	}
	
	private static class ValidationException extends Exception { 
		public ValidationException(String msg) {
			super(msg);
		}
	}
}
