package org.honeynet.hbbackend.hpfeeds;

public class CachedValuesBinary {
	public final long binaryId;
	public final boolean binaryStored;
	
	public CachedValuesBinary(long binaryId, boolean stored) {
		this.binaryId = binaryId;
		this.binaryStored = stored;
	}
	
}