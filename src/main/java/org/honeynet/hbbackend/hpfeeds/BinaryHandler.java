package org.honeynet.hbbackend.hpfeeds;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.annotation.Resource;
import javax.ejb.ActivationConfigProperty;
import javax.ejb.EJBException;
import javax.ejb.MessageDriven;
import javax.ejb.MessageDrivenContext;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.Topic;
import javax.resource.ResourceException;
import javax.sql.DataSource;
import javax.transaction.Synchronization;
import javax.transaction.TransactionSynchronizationRegistry;

import org.honeynet.hpfeeds.Hpfeeds;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xadisk.bridge.proxies.interfaces.XAFileOutputStream;
import org.xadisk.connector.outbound.XADiskConnection;
import org.xadisk.connector.outbound.XADiskConnectionFactory;
import org.xadisk.filesystem.exceptions.ClosedStreamException;
import org.xadisk.filesystem.exceptions.FileAlreadyExistsException;
import org.xadisk.filesystem.exceptions.FileNotExistsException;
import org.xadisk.filesystem.exceptions.FileUnderUseException;
import org.xadisk.filesystem.exceptions.InsufficientPermissionOnFileException;
import org.xadisk.filesystem.exceptions.LockingFailedException;
import org.xadisk.filesystem.exceptions.NoTransactionAssociatedException;


//@MessageDriven(
//activationConfig = {
//		@ActivationConfigProperty(propertyName="channel", propertyValue="honeeebox.binary.test")
//})
@MessageDriven
public class BinaryHandler implements Hpfeeds.MessageHandler {
	private static Logger log = LoggerFactory.getLogger(BinaryHandler.class);
	
	@Resource
    private TransactionSynchronizationRegistry tsr;
	
	@Resource
    private MessageDrivenContext mctx;

	@Resource(mappedName="jdbc/hbbackend")
	private DataSource ds;
	
	@Resource(mappedName="jms/ConnectionFactory")
	private ConnectionFactory jmsConnectionFactory;
	
	@Resource(mappedName="jms/new_binary_stored")
	private Topic jms_new_binary_stored;
	
	@Resource(mappedName="xadisk/ConnectionFactory")
	private XADiskConnectionFactory xaDiskConnectionFactory;
	
	@Resource(name="storageDir")
	private String storageDir;
	
	
	@Override
	public void onMessage(String ident, String chan, ByteBuffer msg) {
		log.trace("onMessage()");
	
		//TODO check chan?
		
		Connection db = null;
		XADiskConnection xadisk = null;
		javax.jms.Connection mq = null;
		Session sess = null;
		
		
		try {
			int binarySize = msg.remaining();
			String md5;
			Long binaryId = 0l;
			boolean binaryStored = false;

			PreparedStatement pStmt;
			ResultSet queryRes;
			
			
			// md5 for msg
			MessageDigest digest = MessageDigest.getInstance("MD5");
			digest.update(msg.duplicate());
			md5 = new BigInteger(1, digest.digest()).toString(16);
			// pad
			if (md5.length() < 32) {
				StringBuilder sb = new StringBuilder(32);
				int i = 32 - md5.length();
				while (i > 0) {
					sb.append("0");
					i--;
				}
				sb.append(md5);
				md5 = sb.toString();
			}
			
			
			db = ds.getConnection();
			
			CacheUpdater cacheUpdater = new CacheUpdater();
			CachedValuesBinary cachedValuesBinary;
			
			cachedValuesBinary = Cache.binaries.get(md5);
			if (cachedValuesBinary != null) {
				binaryId = cachedValuesBinary.binaryId;
				binaryStored = cachedValuesBinary.binaryStored;
			}
			else {
				pStmt = db.prepareStatement("select id, stored from binaries where md5 = ? limit 1");
				pStmt.setString(1, md5);
				queryRes = pStmt.executeQuery();
				if (queryRes.next()) {
					binaryId = queryRes.getLong(1);
					binaryStored = queryRes.getBoolean(2);
				}
				queryRes.close();
				pStmt.close();
				
				if (binaryId == 0) {
					log.info("binary: {}: unknown: md5={} size={}", new Object[]{ ident, md5, binarySize });
					return;
				}
			}
			
			if (binaryStored) {				
				// add to cache
				if (cachedValuesBinary == null) {
					cacheUpdater.binaries = new CachedPair<String, CachedValuesBinary>(md5, new CachedValuesBinary(binaryId, true));
					tsr.registerInterposedSynchronization(cacheUpdater);
				}
				
				log.info("binary: {}: already stored: id={} md5={} size={}", new Object[]{ ident, binaryId, md5, binarySize });
				return;
			}
			else {
				log.info("binary: {}: new: id={} md5={} size={}", new Object[]{ ident, binaryId, md5, binarySize });
				
				// store binary
				
				xadisk = xaDiskConnectionFactory.getConnection();
				
				// create dirs from md5
				String path = storageDir;
				for (String pathPart : Storage.pathAsArray(md5)) {
					path += File.separator + pathPart;
					File dir = new File(path);
					try {
						xadisk.createFile(dir, true);
					} catch (FileAlreadyExistsException e) {
						continue;
					} catch (FileNotExistsException e) {
						// this shouldn't happen
						log.error("parent dir for dir doesn't exist: {}", dir.getAbsolutePath());
						throw new EJBException(e);
					}
				}
				
				// write to disk
				try {
					File file = new File(path + File.separator + md5 + ".bin");
					
					xadisk.createFile(file, false); //TODO set lock timeout?
					XAFileOutputStream out = xadisk.createXAFileOutputStream(file, true);
					InputStream in = new ByteBufferInputStream(msg.duplicate());
					
					int oneByte, n = 0;
					while ((oneByte = in.read()) != -1) {
						out.write(oneByte);
						n++;
					}
					log.debug("writing {} bytes", n);
					out.close();
					
					
					// then update record
					pStmt = db.prepareStatement("update binaries set stored = ?, filesize = ? where id = ?");
					pStmt.setBoolean(1, true);
					pStmt.setInt(2, binarySize);
					pStmt.setLong(3, binaryId);
					pStmt.executeUpdate();
					pStmt.close();
					
					// add to / update cache
					cacheUpdater.binaries = new CachedPair<String, CachedValuesBinary>(md5, new CachedValuesBinary(binaryId, true));
					tsr.registerInterposedSynchronization(cacheUpdater);
					
					// send message
					mq = jmsConnectionFactory.createConnection();
					sess = mq.createSession(true, 0);
					MessageProducer prod;
					Message jmsMsg;
					
					prod = sess.createProducer(jms_new_binary_stored);
					jmsMsg = sess.createTextMessage();
					jmsMsg.setLongProperty("id", binaryId);
					jmsMsg.setStringProperty("md5", md5);
					prod.send(jmsMsg);
					prod.close();
					log.debug("sending msg to new_binary_stored | id={} md5={}", binaryId, md5);
					
					log.debug("binary submission complete | ident={} id={} md5={} size={}", new Object[]{ ident, binaryId, md5, binarySize });
					return;
					
				} catch (FileAlreadyExistsException e) {
					// landing here if a concurrent transaction has stored the same binary just before us
					// additional DB/file system consistency checks could be
					// made (?), but rather should not be necessary
					log.debug("got FileAlreadyExistsException: binary already stored by another transaction(?) | ident={} id={} md5={} size={}", new Object[]{ ident, binaryId, md5, binarySize, e });
					return;
					
				} catch (IOException e) {
					log.error("got IOException", e);
					throw new EJBException(e);
				} catch (FileNotExistsException e) {
					// this shouldn't happen
					log.error("parent dir for file doesn't exist: {}", e);
					throw new EJBException(e);
				} catch (FileUnderUseException e) {
					// this shouldn't happen
					log.error("got FileUnderUseException", e);
					throw new EJBException(e);
				} catch (ClosedStreamException e) {
					log.error("got ClosedStreamException", e);
					throw new EJBException(e);
				}
			}
			
		} catch (NoSuchAlgorithmException e) {
			log.error("got NoSuchAlgorithmException", e);
			throw new EJBException(e);
		} catch (SQLException e) {
			log.error("got SQLException", e);
			throw new EJBException(e);
		} catch (ResourceException e) {
			log.error("got ResourceException", e);
			throw new EJBException(e);
		} catch (LockingFailedException e) {
			log.error("got LockingFailedException", e);
			throw new EJBException(e);
		} catch (NoTransactionAssociatedException e) {
			log.error("got NoTransactionAssociatedException", e);
			throw new EJBException(e);
		} catch (InsufficientPermissionOnFileException e) {
			log.error("got InsufficientPermissionOnFileException", e);
			throw new EJBException(e);
		} catch (InterruptedException e) {
			log.error("got InterruptedException", e);
			throw new EJBException(e);
		} catch (JMSException e) {
			log.error("got JMSException", e);
			throw new EJBException(e);
		} finally {
			try {
				if (sess != null) sess.close();
				if (mq != null)	mq.close();
				if (xadisk != null) xadisk.close();
				if (db != null)	db.close();
			}
			catch (JMSException e) {
				log.error("got JMSException while closing resource", e);
			}
			catch (SQLException e) {
				log.error("got SQLException while closing resource", e);
			}
		}
		
	}
	
	
	public static class Storage {
		public static String path(String md5) {
			return
				md5.substring(0, 2+1) + File.separator +
				md5.substring(30, 31+1) + File.separator +
				md5; 
		}
		
		public static String[] pathAsArray(String md5) {
			return new String[] {
					md5.substring(0, 2+1),
					md5.substring(30, 31+1),
					md5
			};
		}
	}
	
	
	private static class CacheUpdater implements Synchronization {
		protected CachedPair<String, CachedValuesBinary> binaries;

		@Override
		public void afterCompletion(int status) {
			if (status == javax.transaction.Status.STATUS_COMMITTED) {
				if(binaries != null) {
					Cache.binaries.put(binaries.key, binaries.value);
				}
			}
		}

		@Override
		public void beforeCompletion() {}
	}

}