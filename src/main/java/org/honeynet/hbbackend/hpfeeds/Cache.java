package org.honeynet.hbbackend.hpfeeds;

import com.googlecode.concurrentlinkedhashmap.ConcurrentLinkedHashMap;

public class Cache {
	public static final ConcurrentLinkedHashMap<String, Long> idents;
	public static final ConcurrentLinkedHashMap<String, CachedValuesBinary> binaries;
	public static final ConcurrentLinkedHashMap<String, Boolean> ips_source;
	public static final ConcurrentLinkedHashMap<String, Boolean> ips_target;
	
	static {
		idents = new ConcurrentLinkedHashMap.Builder<String, Long>()
    	.maximumWeightedCapacity(1000)
    	.build();
		
		binaries = new ConcurrentLinkedHashMap.Builder<String, CachedValuesBinary>()
    	.maximumWeightedCapacity(2000)
    	.build();
		
		ips_source = new ConcurrentLinkedHashMap.Builder<String, Boolean>()
    	.maximumWeightedCapacity(5000)
    	.build();
		
		ips_target = new ConcurrentLinkedHashMap.Builder<String, Boolean>()
    	.maximumWeightedCapacity(5000)
    	.build();
	}
	
}
