package org.honeynet.hbbackend.hpfeeds;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.annotation.Resource;
import javax.ejb.ActivationConfigProperty;
import javax.ejb.EJBException;
import javax.ejb.MessageDriven;
import javax.ejb.MessageDrivenContext;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.jms.Topic;
import javax.sql.DataSource;
import javax.transaction.Synchronization;
import javax.transaction.TransactionSynchronizationRegistry;

import org.codehaus.jackson.map.ObjectMapper;
import org.honeynet.hpfeeds.Hpfeeds;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


//@MessageDriven(
//activationConfig = {
//		@ActivationConfigProperty(propertyName="channel", propertyValue="dionaea.capture")
//})
@MessageDriven
public class AttackHandler implements Hpfeeds.MessageHandler {
	private static Logger log = LoggerFactory.getLogger(AttackHandler.class);
	
	@Resource
    private TransactionSynchronizationRegistry tsr;
	
	@Resource
    private MessageDrivenContext mctx;
	
	@Resource(mappedName="jdbc/hbbackend")
	private DataSource ds;
	
	@Resource(mappedName="jms/ConnectionFactory")
	private ConnectionFactory jmsConnectionFactory;
	
	@Resource(mappedName="jms/new_attack")
	private Topic jms_new_attack;
	@Resource(mappedName="jms/new_binary")
	private Topic jms_new_binary;
	@Resource(mappedName="jms/new_ip")
	private Topic jms_new_ip;

	private ObjectMapper jsonObjectMapper = new ObjectMapper();
	
	
	@Override
	public void onMessage(String ident, String chan, ByteBuffer msg) {
		log.trace("onMessage()");
		
		//TODO check chan?

		AttackJson attack;
		
		try {
			attack = jsonObjectMapper.readValue(new ByteBufferInputStream(msg), AttackJson.class);
		}
		catch (IOException e) {
			log.error("got IOException", e);
			throw new EJBException(e);
		}
				
		
		Connection db = null;
		javax.jms.Connection mq = null;
		Session sess = null;
		
		try {
			log.info("attack: {}: {}", ident, attack );
			
			long attackId, identId, binaryId, ts;
			boolean binaryStored, identNew, binaryNew, sourceIpNew, targetIpNew;
			
			PreparedStatement pStmt;
			ResultSet queryRes;
			
			db = ds.getConnection();
			
			CacheUpdater cacheUpdater = new CacheUpdater();
			
			// handle ident
			Long cachedIdentId = Cache.idents.get(ident);
			if (cachedIdentId != null) {
				identId = cachedIdentId;
				
				identNew = false;
			}
			else {
				pStmt = db.prepareStatement("select * from safe_insert_ident(?)");
				pStmt.setString(1, ident);
				queryRes = pStmt.executeQuery();
				queryRes.next();
				
				identId = queryRes.getLong(1);
				identNew = queryRes.getBoolean(2); // ret_inserted
				
				queryRes.close();
				pStmt.close();
				
				cacheUpdater.indents = new CachedPair<String, Long>(ident, identId);
			}
			if (identNew) {					
				log.debug("new ident | ident={} id={}", ident, identId);
			} else {
				log.debug("known ident | ident={} id={}", ident, identId);
			}
			
			// handle binary
			CachedValuesBinary cachedValuesBinary = Cache.binaries.get(attack.md5);
			if (cachedValuesBinary != null) {
				binaryId = cachedValuesBinary.binaryId;
				binaryStored = cachedValuesBinary.binaryStored;
				
				binaryNew = false;
			}
			else {
				pStmt = db.prepareStatement("select * from safe_insert_binary(?,?,?,?,?)");
				pStmt.setString(1, attack.md5);
				pStmt.setString(2, attack.sha512);
				pStmt.setNull(3, java.sql.Types.VARCHAR);
				pStmt.setNull(4, java.sql.Types.INTEGER);
				pStmt.setBoolean(5, false);
				queryRes = pStmt.executeQuery();
				queryRes.next();
				
				binaryId = queryRes.getLong(1); // ret_id
				binaryStored = queryRes.getBoolean(3); // ret_stored
				binaryNew = queryRes.getBoolean(2); // ret_inserted
				
				queryRes.close();
				pStmt.close();
				
				cacheUpdater.binaries = new CachedPair<String, CachedValuesBinary>(attack.md5, new CachedValuesBinary(binaryId, binaryStored));
			}
			if (binaryNew) {					
				log.debug("new binary | id={} md5={} stored={}", new Object[]{ binaryId, attack.md5, binaryStored });
			}
			else {
				log.debug("known binary | id={} md5={} stored={} ", new Object[]{ binaryId, attack.md5, binaryStored });
			}
			
			// necessary locks
			pStmt = db.prepareStatement("select 1 from idents where id = ? for update");
			pStmt.setLong(1, identId);
			pStmt.execute();
			pStmt.close();
			pStmt = db.prepareStatement("select 1 from binaries where id = ? for update");
			pStmt.setLong(1, binaryId);
			pStmt.execute();
			pStmt.close();
			
			 // insert attack
			pStmt = db.prepareStatement(
					"insert into attacks (ident_id, binary_id, source_ip, source_port, target_ip, target_port, url)" +
					" values (?,?,inet(?),?,inet(?),?,?)" +
					" returning id, transaction_timestamp()"
					);
			pStmt.setLong(1, identId);
			pStmt.setLong(2, binaryId);
			pStmt.setString(3, attack.saddr);
			pStmt.setInt(4, attack.sport);
			pStmt.setString(5, attack.daddr);
			pStmt.setInt(6, attack.dport);
			pStmt.setString(7, attack.url);
			queryRes = pStmt.executeQuery();
			queryRes.next();
			
			attackId = queryRes.getLong(1);
			ts = queryRes.getTimestamp(2).getTime();
			
			queryRes.close();
			pStmt.close();
			log.debug("inserted attack | id={} ident={} ident_id={} binary_id={} binary.stored={}", new Object[]{ attackId, ident, identId, binaryId, binaryStored });
			
			
			// insert ips
			if (Cache.ips_source.containsKey(attack.saddr)) {
				sourceIpNew = false;
			}
			else {
				pStmt = db.prepareStatement("select * from safe_insert_ip_source(inet(?))");
				pStmt.setString(1, attack.saddr);
				queryRes = pStmt.executeQuery();
				queryRes.next();
				
				sourceIpNew = queryRes.getBoolean(2); // ret_inserted
				
				queryRes.close();
				pStmt.close();
				
				cacheUpdater.ips_source = new CachedPair<String, Boolean>(attack.saddr, true);
			}
			if (sourceIpNew) {				
				log.debug("new source ip | ip={} attack.id={}", attack.saddr, attackId);
			}
			else {
				log.debug("known source ip | ip={} attack.id={}", attack.saddr, attackId);
			}
			
			
			if (Cache.ips_target.containsKey(attack.daddr)) {
				targetIpNew = false;
			}
			else {
				pStmt = db.prepareStatement("select * from safe_insert_ip_target(inet(?))");
				pStmt.setString(1, attack.daddr);
				queryRes = pStmt.executeQuery();
				queryRes.next();
				
				targetIpNew = queryRes.getBoolean(2); // ret_inserted
				
				queryRes.close();
				pStmt.close();
				
				cacheUpdater.ips_target = new CachedPair<String, Boolean>(attack.daddr, true);
			}
			if (targetIpNew) {
				log.debug("new target ip | ip={} attack.id={}", attack.daddr, attackId);
			}
			else {
				log.debug("known target ip | ip={} attack.id={}", attack.daddr, attackId);
			}
			
			
			// stat updates
			pStmt = db.prepareStatement("select update_ident_stats(?)");
			pStmt.setLong(1, identId);
			pStmt.execute();
			pStmt.close();
			pStmt = db.prepareStatement("select update_binary_stats(?)");
			pStmt.setLong(1, binaryId);
			pStmt.execute();
			pStmt.close();
			pStmt = db.prepareStatement("select update_ip_source_stats(inet(?))");
			pStmt.setString(1, attack.saddr);
			pStmt.execute();
			pStmt.close();
			pStmt = db.prepareStatement("select update_ip_target_stats(inet(?))");
			pStmt.setString(1, attack.daddr);
			pStmt.execute();
			pStmt.close();
			
			// add to cache
			tsr.registerInterposedSynchronization(cacheUpdater);
			
			// send messages
			mq = jmsConnectionFactory.createConnection();
			sess = mq.createSession(true, 0);
			MessageProducer prod;
			TextMessage jmsMsg;
			
			// new_attack
			prod = sess.createProducer(jms_new_attack);
			jmsMsg = sess.createTextMessage();
			jmsMsg.setJMSType("hpfeeds");
			jmsMsg.setStringProperty("channel", chan);
			jmsMsg.setStringProperty("ident", ident);
			jmsMsg.setStringProperty("timestamp",Long.toString(ts));
			jmsMsg.setStringProperty("id", Long.toString(attackId));
			prod.send(jmsMsg);
			prod.close();
			log.debug("sending msg to new_attack | id={}", Long.toString(attackId));
			
			// new_binary
			if (binaryNew) {
				prod = sess.createProducer(jms_new_binary);
				jmsMsg = sess.createTextMessage();
				jmsMsg.setStringProperty("id", Long.toString(binaryId));
				jmsMsg.setStringProperty("md5", attack.md5);
				prod.send(jmsMsg);
				prod.close();
				log.debug("sending msg to new_binary | id={} md5={}", binaryId, attack.md5);
			}
			
			// new_ip
			if (sourceIpNew) { //TODO target ip too?
				prod = sess.createProducer(jms_new_ip);
				jmsMsg = sess.createTextMessage();
				jmsMsg.setJMSType("source");
				jmsMsg.setStringProperty("ip", attack.saddr);
				prod.send(jmsMsg);
				prod.close();
				log.debug("sending msg to new_ip | ip={}", attack.saddr);
			}
			
			log.debug("attack submission complete | ident={} id={} ident_id={} binary_id={} binary.stored={}", new Object[]{ ident, attackId, identId, binaryId, binaryStored });
			return;
		}
		catch (SQLException e) {
			log.error("got SQLException", e);
			throw new EJBException(e);
		}
		catch (JMSException e) {
			log.error("got JMSException", e);
			throw new EJBException(e);
		}
		finally {
			try {
				if (sess != null) sess.close();
				if (mq != null)	mq.close();
				if (db != null)	db.close();
			}
			catch (JMSException e) {
				log.error("got JMSException while closing resource", e);
			}
			catch (SQLException e) {
				log.error("got SQLException while closing resource", e);
			}
		}
	}
	
	
	private static class CacheUpdater implements Synchronization {
		protected CachedPair<String, Long> indents;
		protected CachedPair<String, CachedValuesBinary> binaries;
		protected CachedPair<String, Boolean> ips_source;
		protected CachedPair<String, Boolean> ips_target;
		
		@Override
		public void afterCompletion(int status) {	
			if (status == javax.transaction.Status.STATUS_COMMITTED) {
				if(indents != null) {
					Cache.idents.put(indents.key, indents.value);
				}
				if(binaries != null) {
					Cache.binaries.put(binaries.key, binaries.value);
				}
				if(ips_source != null) {
					Cache.ips_source.put(ips_source.key, ips_source.value);
				}
				if(ips_target != null) {
					Cache.ips_target.put(ips_target.key, ips_target.value);
				}
			}
		}

		@Override
		public void beforeCompletion() {}
		
	}
	
}