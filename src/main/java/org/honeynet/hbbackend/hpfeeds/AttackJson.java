package org.honeynet.hbbackend.hpfeeds;

public class AttackJson {
	public String saddr, daddr, md5, sha512, url;
	public int sport, dport;

	@Override
	public String toString() {
		//TODO escape commas
		return String.format("%s,%s,%s,%s,%s,%s,%s", saddr, sport, daddr, dport, md5, sha512, url);
	}
	
}
