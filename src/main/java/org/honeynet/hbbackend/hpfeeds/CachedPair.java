package org.honeynet.hbbackend.hpfeeds;

public class CachedPair<K, V> {
	public final K key;
	public final V value;
	
	public CachedPair(K key, V value) {
		this.key = key;
		this.value = value;
	}
	
}
