package org.honeynet.hbbackend.hpfeeds;

import java.io.IOException;
import java.io.InputStream;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;


public class ByteBufferInputStream extends InputStream {
	private ByteBuffer buf;
	
	public ByteBufferInputStream(ByteBuffer buf) {
		this.buf = buf;
	}

	@Override
	public int read() throws IOException {
		try {
			return buf.get() & 0xff;
		}
		catch (BufferUnderflowException e) {
			return -1;
		}
	}
	
	//TODO implement other read methods?
}