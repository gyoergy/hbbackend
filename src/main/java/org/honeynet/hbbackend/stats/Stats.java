package org.honeynet.hbbackend.stats;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;
import javax.ejb.EJBException;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.ejb.Timeout;
import javax.ejb.Timer;
import javax.ejb.TimerConfig;
import javax.ejb.TimerService;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.sql.DataSource;

import org.postgresql.PGConnection;
import org.postgresql.copy.CopyIn;
import org.postgresql.copy.CopyManager;
import org.postgresql.copy.CopyOut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


@Singleton
@Startup
public class Stats {
	private static final int PAUSE = 2000;

	private static Logger log = LoggerFactory.getLogger(Stats.class);
	
	@Resource(mappedName="jdbc/hbbackend")
	private DataSource ds_hbbackend;
	
	@Resource(mappedName="jdbc/hbstats")
	private DataSource ds_hbstats;
	
	@Resource
	TimerService timerService;
	
	
	private boolean stop = false;

	
	@PostConstruct
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	private void start() {
		log.trace("start()");
		TimerConfig tc = new TimerConfig();
		tc.setPersistent(false);
		Timer t = timerService.createSingleActionTimer(1000, tc);
	}
	
	
	@Timeout
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void work() {
		log.trace("work()");
		if (stop) return;
		
		Connection db_hbbackend = null, db_hbstats = null;

		try {
			db_hbbackend = ds_hbbackend.getConnection();
			db_hbbackend.createStatement().execute("set transaction read only");
			db_hbstats = ds_hbstats.getConnection();
			
			MergeStatus status =
				new MergeTask(
						db_hbbackend, db_hbstats,
						"main",
						new String[]{ "merge_agg_main", "merge_ts_main_min" }
					).run();	
			
			new DependentMergeTask(db_hbbackend, db_hbstats, "virustotal", status).run();
			new DependentMergeTask(db_hbbackend, db_hbstats, "asn_shadowserver", status).run();
			new DependentMergeTask(db_hbbackend, db_hbstats, "geoip_shadowserver", status).run();
			
			
			if (!stop) {
				TimerConfig tc = new TimerConfig();
				tc.setPersistent(false);
				Timer t = timerService.createSingleActionTimer(PAUSE, tc);
				log.trace("work() timer created");
			}
			
			log.trace("work() return");
		}
		catch (SQLException e) {
			log.error("got SQLException", e);
			throw new EJBException(e);
		}
		finally {
			try {
				if (db_hbbackend != null) db_hbbackend.close();
				if (db_hbstats != null) db_hbstats.close();
			}
			catch (SQLException e) {
				log.error("got SQLException while closing resource", e);
			}
		}
	}
	
	
	@PreDestroy
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	private void stop() {
		log.trace("stop()");
		stop = true;
    }
	
	
	private static class MergeTask {
		protected String func_copy;
		protected String func_x_log_get;
		protected String func_x_log_insert;
		protected String func_create_table_new;
		protected String func_drop_table_new;
		protected String func_new_get;
		protected String func_merge;
		
		protected Connection source, target;
		protected String name;
		protected String[] mergeFuncs;
		
		
		public MergeTask(Connection source, Connection target, String name, String[] mergeFuncs) {
			func_copy = String.format("hbstats_new_%s", name);
			func_x_log_get = String.format("x_log_%s_get", name);
			func_x_log_insert = String.format("x_log_%s_insert", name);
			func_create_table_new = String.format("create_table_new_%s", name);
			func_drop_table_new = String.format("drop_table_new_%s", name);
			func_new_get = String.format("new_%s_get", name);
			func_merge = String.format("merge_%s", name);

			this.source = source;
			this.target = target;
			this.name = name;
			this.mergeFuncs = mergeFuncs;
		}
		
		
		public MergeStatus run() throws SQLException {
			log.trace("run()");

			// get last id in log
			long lastId = getLastIdInLog();
			log.debug("{}: begin - last_id={}", name, lastId);
			
			// create temp table
			String tempTable = createTempTable();
			
			// copy new records
			copy(lastId, tempTable);
			
			// get last/first id and row count
			MergeStatus status = getCopiedStatus();
			
			if (status.lastId == 0) {
				dropTempTable();
				log.trace("merge() return");
				return new MergeStatus(0, 0, 0);
			}
			
			// merge
			log.debug("{}: merging {} new rows | last_id={} first_id={}", new Object[] { name, status.rows, status.lastId, status.firstId });
			merge();
			log.debug("{}: merge complete", name);
			
			// log
			log(status);
			
			// drop temp table;
			dropTempTable();
			
			log.trace("run() return");
			return status;
		}
		
		
		protected long getLastIdInLog() throws SQLException {
			CallableStatement proc;
			long lastId;

			proc = target.prepareCall(String.format("{ call %s( ?) }", func_x_log_get));
			proc.registerOutParameter(1, java.sql.Types.BIGINT);
			proc.execute();
			lastId = proc.getLong(1);
			proc.close();
			
			return lastId;
		}
		
		protected String createTempTable() throws SQLException {
			CallableStatement proc;
			String tempTable;
			
			proc = target.prepareCall(String.format("{ call %s( ?) }", func_create_table_new));
			proc.registerOutParameter(1, java.sql.Types.VARCHAR);
			proc.execute();
			tempTable = proc.getString(1);
			proc.close();
			
			return tempTable;
		}
		
		protected void dropTempTable() throws SQLException {
			CallableStatement proc;
			
			proc = target.prepareCall(String.format("{ call %s() }", func_drop_table_new));
			proc.execute();
			proc.close();
		}
		
		protected MergeStatus getCopiedStatus() throws SQLException {
			CallableStatement proc;
			long lastId, firstId, rows;
			
			proc = target.prepareCall(String.format("{ call %s( ?,?,?) }", func_new_get));
			proc.registerOutParameter(1, java.sql.Types.BIGINT);
			proc.registerOutParameter(2, java.sql.Types.BIGINT);
			proc.registerOutParameter(3, java.sql.Types.BIGINT);
			proc.execute();
			lastId = proc.getLong(1);
			firstId = proc.getLong(2);
			rows = proc.getLong(3);
			proc.close();
			return new MergeStatus(lastId, firstId, rows);
		}
		
		protected void merge() throws SQLException {
			CallableStatement proc;
			
			if (mergeFuncs != null && mergeFuncs.length > 0) {
				for (String mergeFunc : mergeFuncs) {
					proc = target.prepareCall(String.format("{ call %s() }", mergeFunc));
					proc.execute();
					proc.close();
				}
			}
			else {
				proc = target.prepareCall(String.format("{ call %s() }", func_merge));
				proc.execute();
				proc.close();
			}
		}
		
		protected void log(MergeStatus status) throws SQLException {
			CallableStatement proc;
			
			proc = target.prepareCall(String.format("{ call %s(?,?,?, ?) }", func_x_log_insert));
			proc.setLong(1, status.lastId);
			proc.setLong(2, status.firstId);
			proc.setLong(3, status.rows);
			proc.registerOutParameter(4, java.sql.Types.BIGINT);
			proc.execute();
			proc.close();
		}
		
		protected long copy(long lastId, String targetTable) throws SQLException {
			String expr = String.format("%s(%d)", func_copy, lastId);
			return copy(expr, targetTable);
		}
		
		
		protected long copy(String expr, String targetTable) throws SQLException {
			log.trace("copy()");
			
			CopyManager cpm_source = source.unwrap(PGConnection.class).getCopyAPI();
			CopyManager cpm_target = target.unwrap(PGConnection.class).getCopyAPI();
			
			CopyOut cpo = cpm_source.copyOut("copy ( select * from " + expr +" ) to stdout with csv header");
			CopyIn cpi= cpm_target.copyIn("copy " + targetTable + " from stdin with csv header");
			
			byte[] buf;
			long n = 0, rows;
			while ((buf = cpo.readFromCopy()) != null) {
				cpi.writeToCopy(buf, 0, buf.length);
				n += buf.length;
			}
			rows = cpi.endCopy();
			
			log.debug("copy: completed: rows = {}, bytes = {}", rows, n);
			return rows;
		}
	}
	
	
	private static class DependentMergeTask extends MergeTask {
		private MergeStatus status;
		
		public DependentMergeTask(Connection source, Connection target, String name, MergeStatus status) {
			super(source, target, name, null);
			this.status = status;
		}
		
		public MergeStatus run() throws SQLException {
			log.trace("run()");
			
			if (status.lastId == 0) {
				log.debug("{}: got 0 new rows, returning", name);
 				
				log.trace("merge() return");
				return status;
			}
			
			// create temp table
			String tempTable = createTempTable();
			
			// copy new records
			long rows = copy(status, tempTable);
			
			// merge
			log.debug("{}: merging {} new rows | last_id={} first_id={}", new Object[] { name, rows, status.lastId, status.firstId });
			merge();
			log.debug("{}: merge complete", name);
			
			// drop temp table
			dropTempTable();
			
			log.trace("run() return");
			return status;
		}
		
		private long copy(MergeStatus status, String targetTable) throws SQLException {
			String expr = String.format("%s(%d,%d)", func_copy, status.firstId, status.lastId);
			return copy(expr, targetTable);
		}
	}
	
	
	private static class MergeStatus {
		public long lastId, firstId, rows;
		
		public MergeStatus(long lastId, long firstId, long rows) {
			this.lastId = lastId;
			this.firstId = firstId;
			this.rows = rows;
		}
	}
}
