package org.honeynet.hbbackend.hpfeedsra;

import java.nio.ByteBuffer;
import java.util.Collection;
import java.util.concurrent.ConcurrentHashMap;

import javax.resource.spi.UnavailableException;
import javax.resource.spi.endpoint.MessageEndpoint;
import javax.resource.spi.work.Work;
import javax.resource.spi.work.WorkException;
import javax.resource.spi.work.WorkManager;

import org.honeynet.hpfeeds.Hpfeeds;
import org.honeynet.hbbackend.hpfeedsra.inflow.HpfeedsActivation;
import org.honeynet.hbbackend.hpfeedsra.inflow.HpfeedsActivationSpec;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class HpfeedsMessageHandler implements Hpfeeds.MessageHandler {
	private static Logger log = LoggerFactory.getLogger(HpfeedsMessageHandler.class);
	
	private WorkManager workManager;
	private HpfeedsRa hpfeedsRa;
//	private ConcurrentHashMap<String, ConcurrentHashMap<HpfeedsActivationSpec, HpfeedsActivation>> activationsMap;
	
	
	public HpfeedsMessageHandler(HpfeedsRa hpfeedsRa, WorkManager workManager) {
		this.hpfeedsRa = hpfeedsRa;
		this.workManager = workManager;
//		this.activationsMap = activationsMap;
	}
	
	
	@Override
	public void onMessage(String ident, String channel, ByteBuffer msg) {
		log.trace("onMessage()");
		
		Collection<HpfeedsActivation> activations = hpfeedsRa.activationsMap.get(channel).values();
		if (activations.isEmpty()) {
//			log.error("no activations found - should not happen");
			return;
		}
		
		for (HpfeedsActivation a : activations) {
			try {
				MessageEndpoint me = a.getMessageEndpointFactory().createEndpoint(null);
				
				Hpfeeds.MessageHandler messageHandler = null;
				try {
					messageHandler = (Hpfeeds.MessageHandler) me;
				}
				catch (ClassCastException e) {
					log.error("got ClassCastException", e);
					log.error("MessageEndpoint is no Hpfeeds.MessageHandler - should not happen");
					hpfeedsRa.onError(e, this.getClass());
					return;
				}
				
				log.trace("passing work to WorkManager");
				OnMessageWork work = new OnMessageWork(messageHandler, ident, channel, msg);
				workManager.startWork(work);
				
				me.release();
			}
			catch (UnavailableException e) {
				log.error("got UnavailableException", e);
				hpfeedsRa.onError(e, this.getClass());
				return;
			}
			catch (WorkException e) {
				log.error("got WorkException", e);
				hpfeedsRa.onError(e, this.getClass());
				return;
			}
		}
		
	}
	
	
	private static class OnMessageWork implements Work {
		private Hpfeeds.MessageHandler messageHandler;
		private String ident, chan;
		private ByteBuffer msg;
		
		public OnMessageWork(Hpfeeds.MessageHandler messageHandler, String ident, String channel, ByteBuffer msg) {
			this.messageHandler = messageHandler;
			this.ident = ident;
			this.chan = channel;
			this.msg = msg;
		}
		
		@Override
		public void run() {
			messageHandler.onMessage(ident, chan, msg);
		}

		@Override
		public void release() {

		}
		
	}
	
}