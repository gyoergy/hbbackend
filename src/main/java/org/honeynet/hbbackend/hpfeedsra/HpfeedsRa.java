package org.honeynet.hbbackend.hpfeedsra;

import java.io.IOException;
import java.lang.management.ManagementFactory;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ConcurrentHashMap;

import javax.management.InstanceAlreadyExistsException;
import javax.management.InstanceNotFoundException;
import javax.management.MBeanRegistrationException;
import javax.management.MBeanServer;
import javax.management.MalformedObjectNameException;
import javax.management.NotCompliantMBeanException;
import javax.management.ObjectName;
import javax.resource.ResourceException;
import javax.resource.spi.ActivationSpec;
import javax.resource.spi.BootstrapContext;
import javax.resource.spi.ConfigProperty;
import javax.resource.spi.Connector;
import javax.resource.spi.ResourceAdapter;
import javax.resource.spi.ResourceAdapterInternalException;
import javax.resource.spi.UnavailableException;
import javax.resource.spi.endpoint.MessageEndpointFactory;
import javax.resource.spi.work.WorkException;
import javax.resource.spi.work.WorkManager;
import javax.transaction.xa.XAResource;

import org.honeynet.hpfeeds.Hpfeeds;
import org.honeynet.hpfeeds.Hpfeeds.EOSException;
import org.honeynet.hpfeeds.Hpfeeds.InvalidStateException;
import org.honeynet.hpfeeds.Hpfeeds.LargeMessageException;
import org.honeynet.hpfeeds.Hpfeeds.ReadTimeOutException;
import org.honeynet.hbbackend.hpfeedsra.inflow.HpfeedsActivation;
import org.honeynet.hbbackend.hpfeedsra.inflow.HpfeedsActivationSpec;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


@Connector
public class HpfeedsRa implements ResourceAdapter, HpfeedsRaMXBeanInterface, java.io.Serializable {
	private static Logger log = LoggerFactory.getLogger(HpfeedsRa.class);
	
	private static final long serialVersionUID = 1L;
	
	protected static final String STATUS_CONNECTED = "connected"; // connected to broker, but consumer not running
	protected static final String STATUS_RUNNING = "running"; // connected and consumer running
	protected static final String STATUS_STOPPED = "stopped"; // connected, but consumer has been stopped
	protected static final String STATUS_DISCONNECTED = "disconnected"; // disconnected
	protected static final String STATUS_ERROR = "error"; // a error has occurred, but possibly still connected
	protected static final String STATUS_SHUT_DOWN = "shut down"; // resource adapter shut down
	
	@ConfigProperty
	private String host;
	@ConfigProperty
	private Integer port;
	@ConfigProperty
	private String ident;
	@ConfigProperty(confidential = true)
	private String secret;
	@ConfigProperty
	private String channels;
	
	private String status;
	private String lastError;
	private long startTime;
	
	private ObjectName mxbeanObjectName;
	private Object lock = new Object();
	
	private BootstrapContext ctx;
	private WorkManager workManager;
	private Timer timer;
	
	private Hpfeeds hpfeeds;
	private RunWork runWork;
	private String[] channelsArray;
	protected final ConcurrentHashMap<String, ConcurrentHashMap<HpfeedsActivationSpec, HpfeedsActivation>> activationsMap
		= new ConcurrentHashMap<String, ConcurrentHashMap<HpfeedsActivationSpec, HpfeedsActivation>>();
	
	private boolean firstRun = true;	
	
	
	public HpfeedsRa() throws ResourceAdapterInternalException {
		
	}
	
	
	public void setHost(String host) {
		this.host = host;
	}
	
	public void setPort(Integer port) {
		this.port = port;
	}

	public void setIdent(String identifier) {
		this.ident = identifier;
	}

	public void setSecret(String secret) {
		this.secret = secret;
	}
	
	public void setChannels(String channels) {
		this.channels = channels;
	}

	public String getHost() {
		return host;
	}

	public Integer getPort() {
		return port;
	}

	public String getIdent() {
		return ident;
	}

	public String getSecret() {
		return secret;
	}
	
	public String getChannels() {
		return channels;
	}
	
	
	public void start(BootstrapContext ctx) throws ResourceAdapterInternalException {
		log.trace("start()");
		
		if (host == null || port == null || ident == null || secret == null || channels == null || channels.matches("^,*$")) {
			throw new ResourceAdapterInternalException("missing config");
		}
		else if (!(port > 0 && port <= 65535)) {
			throw new ResourceAdapterInternalException("invalid port");
		}
		
		channelsArray = this.channels.split(",");
		
		log.debug("starting resource adapter");
		log.debug("config: host: " + host);
		log.debug("config: port: " + port);
		log.debug("config: ident: " + ident);
		log.debug("config: secret: " + secret.replaceAll(".", "*"));
		for (String c : channelsArray) { log.debug("config: channel: " + c); }
		
		
		for (String c : channelsArray) {
			activationsMap.put(c, new ConcurrentHashMap<HpfeedsActivationSpec, HpfeedsActivation>());
		}
		
		
		this.ctx = ctx;
		workManager = ctx.getWorkManager();
		
		
		try {
			hpfeeds = new Hpfeeds(host, port, ident, secret);
			startTime = System.currentTimeMillis();
			hpfeedsConnectAndSubscribe();
		} catch (ResourceAdapterInternalException e) {
			throw e;
		}
		
		
		synchronized (lock) {
			try {
				MBeanServer mbs = ManagementFactory.getPlatformMBeanServer();
				
				mxbeanObjectName = new ObjectName(String.format(
						"%s:name=%s,id=%s,broker=\"%s:%s\"",
						"org.honeynet.hbbackend.hpfeedsra", //FIXME?
						"hpfeedsra", String.format("@%x@%x", System.identityHashCode(this.getClass().getClassLoader()), System.identityHashCode(this)), //FIXME?
						host, port
						));
				mbs.registerMBean(this, mxbeanObjectName);
			}
			catch (MalformedObjectNameException e) {
				log.error("got MalformedObjectNameException", e);
				hpfeedsDisconnect();
				throw new ResourceAdapterInternalException(e);
			}
			catch (InstanceAlreadyExistsException e) {
				log.error("got InstanceAlreadyExistsException", e);
				hpfeedsDisconnect();
				throw new ResourceAdapterInternalException(e);
			}
			catch (MBeanRegistrationException e) {
				log.error("got MBeanRegistrationException", e);
				hpfeedsDisconnect();
				throw new ResourceAdapterInternalException(e);
			}
			catch (NotCompliantMBeanException e) {
				log.error("got NotCompliantMBeanException", e);
				hpfeedsDisconnect();
				throw new ResourceAdapterInternalException(e);
			}
			
			
			try {
				HpfeedsMessageHandler messageHandler = new HpfeedsMessageHandler(this, workManager);
				HpfeedsErrorHandler errorHandler = new HpfeedsErrorHandler(this);
				runWork = new RunWork(this, hpfeeds, messageHandler, errorHandler, true);
				hpfeedsRun();
			}
			catch (ResourceAdapterInternalException e) {
//				hpfeeds.stop();
				runWork.release();
				hpfeedsDisconnect();
				unregisterMBean();
				status = STATUS_SHUT_DOWN;
			}
		}
	}
	
	
	public void stop() {
		synchronized (lock) {
			log.trace("stop()");
			
			hpfeeds.stop();
			runWork.release();
			status = STATUS_STOPPED;
			hpfeedsDisconnect();
			
			if (timer != null) timer.cancel();
			unregisterMBean();
			status = STATUS_SHUT_DOWN;
			
			log.info("shut down");
		}
	}
	
	
	private void hpfeedsConnectAndSubscribe() throws ResourceAdapterInternalException {
		try {
			hpfeeds.connect();
			hpfeeds.subscribe(channelsArray);
			status = STATUS_CONNECTED;
			log.info("connected to {}:{} | ident={} channels={}", new Object[]{ host, port, ident, channels });
		}
		catch (IOException e) {
			hpfeedsDisconnect();
			log.error("got IOException", e);
			throw new ResourceAdapterInternalException(e);
		}
		catch (EOSException e) {
			hpfeedsDisconnect();
			log.error("got EOSException", e);
			throw new ResourceAdapterInternalException(e);
		}
		catch (ReadTimeOutException e) {
			hpfeedsDisconnect();
			log.error("got EOSException", e);
			throw new ResourceAdapterInternalException(e);
		}
		catch (LargeMessageException e) {
			hpfeedsDisconnect();
			log.error("got LargeMessageException", e);
			throw new ResourceAdapterInternalException(e);
		}
		catch (InvalidStateException e) {
			hpfeedsDisconnect();
			log.error("got InvalidStateException", e);
			throw new ResourceAdapterInternalException(e);
		}
	}
	
	private void hpfeedsRun() throws ResourceAdapterInternalException {
		try {
			workManager.startWork(runWork);
		}
		catch (WorkException e) {
			log.error("got WorkException", e);
			throw new ResourceAdapterInternalException(e);
		}
	}
	
	private void hpfeedsDisconnect() {
		try {
			hpfeeds.disconnect();
			status = STATUS_DISCONNECTED;
		}
		catch (IOException e) {
			log.warn("IOException while disconnecting", e);
		}
	}
	
	protected void onHpfeedsError(String hpfeedsErrorMessage) {
		lastError = "hpfeeds: " + hpfeedsErrorMessage;
		onError(lastError);
	}
	
	protected void onError(Throwable exception, Class cls) {
		lastError = "exception: " + exception.getClass().getSimpleName() + " in " + cls.getSimpleName();
		onError(lastError);
	}
	
	protected void onError(String msg) {
		synchronized (lock) {
			if (status != STATUS_ERROR || status != STATUS_SHUT_DOWN) {
				hpfeeds.stop();
				runWork.release();
				status = STATUS_ERROR;
				
				final String logMsg = "stuck with error: " + msg + " - restart or redeploy";
				
				try {
					if (timer != null) timer.cancel();
					timer = ctx.createTimer();
				}
				catch (UnavailableException e) {
					log.error(logMsg);
					log.error("got UnavailableException while creating timer", e);
					return;
				}
				
				TimerTask tt = new TimerTask() {
					@Override
					public void run() {
						log.error(logMsg);
					}
				};
				timer.schedule(tt, 0, 3000);
			}
		}
	}
	
	
	public void endpointActivation(MessageEndpointFactory endpointFactory, ActivationSpec activationSpec) throws ResourceException {
		log.trace("endpointActivation()");
		
		HpfeedsActivationSpec spec = (HpfeedsActivationSpec) activationSpec;
		String chan = spec.getChannel();
		
		if (!activationsMap.containsKey(chan)) {
			throw new ResourceException("unknown channel: " + chan);
		}
		
		HpfeedsActivation activation = new HpfeedsActivation(this, endpointFactory, spec);
		activationsMap.get(chan).put(spec, activation);
		activation.start();
		
		synchronized (lock) { 
			if (firstRun) {
				boolean gotEmpty = false;
				for (ConcurrentHashMap<HpfeedsActivationSpec, HpfeedsActivation> m : activationsMap.values()) {
					if (m.isEmpty()) return;
				}
				if (!gotEmpty) {
					if (status == STATUS_CONNECTED) {
						runWork.signal();
						status = STATUS_RUNNING;
						firstRun = false;
					}
				}
			}
		}
	}
	
	
	//FIXME stop run() at deactivation?
	public void endpointDeactivation(MessageEndpointFactory endpointFactory, ActivationSpec activationSpec) {
		log.trace("endpointDeactivation()");

		HpfeedsActivationSpec spec = (HpfeedsActivationSpec) activationSpec;
		String chan = spec.getChannel();
		
		if (!activationsMap.containsKey(chan)) {
			log.warn("unknown channel in endpointDeactivation()");
			return;
		}
		
		HpfeedsActivation activation = activationsMap.get(chan).remove(spec);
		if (activation != null) {
			activation.stop();
		}
	}
	
	
	public XAResource[] getXAResources(ActivationSpec[] specs) throws ResourceException {
		return null;
	}
	
	
	
	
	private void unregisterMBean() {
		try {
			MBeanServer mbs = ManagementFactory.getPlatformMBeanServer();
			if (mbs.isRegistered(mxbeanObjectName)) {
				mbs.unregisterMBean(mxbeanObjectName);
			}
		}
		catch (MBeanRegistrationException e) {
			log.error("got MBeanRegistrationException", e);
		}
		catch (InstanceNotFoundException e) {
			log.error("got MBeanRegistrationException", e);
		}
	}
	
	
	
	
	@Override
	public String getStatus() {
		return status;
	}
	
	@Override
	public long getUptime() {
		return System.currentTimeMillis() - startTime;
	}
	
	@Override
	public String getLastError() {
		return lastError;
	}
	
	@Override
	public long getReadMessages() {
		return hpfeeds.getReadMessages();
	}
	
	@Override
	public long getReadBytes() {
		return hpfeeds.getReadBytes();
	}
	
	@Override
	public long getWrittenMessages() {
		return hpfeeds.getWrittenMessages();
	}
	
	@Override
	public long getWrittenBytes() {
		return hpfeeds.getWrittenBytes();
	}
	
	
	
	@Override
	public void connectAndStart() throws Exception {
		synchronized (lock) {
			if (status != STATUS_DISCONNECTED) {
				if (status == STATUS_ERROR) {
					throw new Exception("not disconnected"); //FIXME?
				}
				throw new Exception("not disconnected");
			}
			if (status == STATUS_SHUT_DOWN) {
				throw new Exception("resource adapter shut down");
			}
			
			try {
				hpfeedsConnectAndSubscribe();
			}
			catch (ResourceAdapterInternalException e) {
				throw new Exception(e.getCause().toString());
			}
			try {
				runWork.setWaitForSignal(false);
				runWork.unRelease();
				hpfeedsRun();
				status = STATUS_RUNNING;
				firstRun = false;
			}
			catch (ResourceAdapterInternalException e) {
				throw new Exception(e.getCause().toString());
			}
		}
	}
	
	@Override
	public void stopAndDisconnect() throws Exception {
		synchronized (lock) {
			if (status == STATUS_DISCONNECTED) {
				throw new Exception("not connected");
			}
			if (status == STATUS_SHUT_DOWN) {
				throw new Exception("resource adapter shut down");
			}
			
			try {
				hpfeeds.stop();
				runWork.release();
				if (status == STATUS_ERROR) timer.cancel();
				status = STATUS_STOPPED;
				hpfeeds.disconnect();
				status = STATUS_DISCONNECTED;
			}
			catch (IOException e) {
				throw new Exception(e);
			}
		}
	}
	
//	@Override
//	public void dropActivations() throws Exception {
//		
//	}
}