package org.honeynet.hbbackend.hpfeedsra;

import java.nio.ByteBuffer;
import java.nio.charset.CharacterCodingException;

import org.honeynet.hpfeeds.Hpfeeds;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class HpfeedsErrorHandler implements Hpfeeds.ErrorHandler {
	private static Logger log = LoggerFactory.getLogger(HpfeedsErrorHandler.class);
	
	private HpfeedsRa hpfeedsRa;
	
	public HpfeedsErrorHandler(HpfeedsRa hpfeedsRa) {
		this.hpfeedsRa = hpfeedsRa;
	}

	@Override
	public void onError(ByteBuffer msg) {
		try {
			String msgString = Hpfeeds.decodeString(msg);
			hpfeedsRa.onHpfeedsError(msgString);
			log.error("hpfeeds error msg: {}", msgString);
		}
		catch (CharacterCodingException e) {
			hpfeedsRa.onError(e, this.getClass());
			log.error("got CharacterCodingException", e);
			return;
		}
	}
	
}