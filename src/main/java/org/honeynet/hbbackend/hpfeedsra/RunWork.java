package org.honeynet.hbbackend.hpfeedsra;

import java.io.IOException;
import java.util.concurrent.SynchronousQueue;

import javax.resource.spi.ResourceAdapterInternalException;
import javax.resource.spi.work.Work;

import org.honeynet.hpfeeds.Hpfeeds;
import org.honeynet.hpfeeds.Hpfeeds.EOSException;
import org.honeynet.hpfeeds.Hpfeeds.ErrorHandler;
import org.honeynet.hpfeeds.Hpfeeds.InvalidStateException;
import org.honeynet.hpfeeds.Hpfeeds.LargeMessageException;
import org.honeynet.hpfeeds.Hpfeeds.MessageHandler;
import org.honeynet.hpfeeds.Hpfeeds.ReadTimeOutException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class RunWork implements Work {
	private static Logger log = LoggerFactory.getLogger(RunWork.class);
	
	private boolean release = false;
	
	private HpfeedsRa hpfeedsRa;
	
	private Hpfeeds hpfeeds;
	private MessageHandler messageHandler;
	private ErrorHandler errorHandler;
	
	private boolean waitForSignal;
	private SynchronousQueue<Integer> syncQueue;
	
	
	public RunWork(HpfeedsRa hpfeedsRa, Hpfeeds hpfeeds, Hpfeeds.MessageHandler messageHandler, Hpfeeds.ErrorHandler errorHandler, boolean waitForSignal) {
		this.hpfeedsRa = hpfeedsRa;
		this.hpfeeds = hpfeeds;
		this.messageHandler = messageHandler;
		this.errorHandler = errorHandler;
		
		this.waitForSignal = waitForSignal;
		if (waitForSignal) {
			syncQueue = new SynchronousQueue<Integer>();
		}
	}
	
	
	public boolean signal() {
		if (waitForSignal) {
			try {
				syncQueue.put(1);
				waitForSignal = false;
				return true;
			}
			catch (InterruptedException e) {
				if (release) return false;
				hpfeedsRa.onError(e, this.getClass());
				log.error("got InterruptedException - should not happen", e);
				return false;
			}
		}
		else {
			return false;
		}
	}
	
	
	@Override
	public void run() {
		if (release) return;
		
		if (waitForSignal) {
			try {
				int r = syncQueue.take();
				if (r == 0) {
					return;
				}
			}
			catch (InterruptedException e) {
				if (release) return;
				hpfeedsRa.onError(e, this.getClass());
				log.error("got InterruptedException - should not happen", e);
				return;
			}
		}
		
		try {
			if (release) return;
			
			log.debug("starting hpfeeds run()");
			
			hpfeeds.run(messageHandler, errorHandler);
		}
		catch (IOException e) {
			if (release) return;
			hpfeedsRa.onError(e, this.getClass());
			log.error("got IOException", e);
			return;
		}
		catch (EOSException e) {
			if (release) return;
			hpfeedsRa.onError(e, this.getClass());
			log.error("got EOSException", e);
			return;
		}
		catch (ReadTimeOutException e) {
			if (release) return;
			hpfeedsRa.onError(e, this.getClass());
			log.error("got ReadTimeOutException", e);
			return;
		}
		catch (LargeMessageException e) {
			if (release) return;
			hpfeedsRa.onError(e, this.getClass());
			log.error("got LargeMessageException", e);
			return;
		}
		catch (InvalidStateException e) {
			if (release) return;
			hpfeedsRa.onError(e, this.getClass());
			log.error("got InvalidStateException", e);
			return;
		}
		
	}
	
	@Override
	public void release() {
		release = true;
		syncQueue.offer(0);
	}
	
	public void unRelease() {
		release = false;
	}
	
	
	public void setWaitForSignal(boolean waitForSignal) {
		this.waitForSignal = waitForSignal;
	}
	
	public Hpfeeds.MessageHandler getMessageHandler() {
		return messageHandler;
	}
	
	public Hpfeeds.ErrorHandler getErrorHandler() {
		return errorHandler;
	}
	
}