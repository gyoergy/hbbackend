package org.honeynet.hbbackend.hpfeedsra;

import javax.management.MXBean;

@MXBean
public interface HpfeedsRaMXBeanInterface {
	String getStatus();
	long getUptime();
	String getLastError();
	
	long getReadMessages();
	long getReadBytes();
	long getWrittenMessages();
	long getWrittenBytes();
	
	void connectAndStart() throws Exception;
	void stopAndDisconnect() throws Exception;
//	void dropActivations() throws Exception;
}