package org.honeynet.hbbackend.hpfeedsra.inflow;

import org.honeynet.hbbackend.hpfeedsra.HpfeedsRa;

import javax.resource.ResourceException;
import javax.resource.spi.endpoint.MessageEndpointFactory;


public class HpfeedsActivation {
	private HpfeedsRa ra;
	private HpfeedsActivationSpec spec;
	private MessageEndpointFactory endpointFactory;

	/**
	 * Default constructor
	 * 
	 * @exception ResourceException
	 *                Thrown if an error occurs
	 */
	public HpfeedsActivation() throws ResourceException {
		this(null, null, null);
	}

	/**
	 * Constructor
	 * 
	 * @param ra
	 *            HpfeedsRA
	 * @param endpointFactory
	 *            MessageEndpointFactory
	 * @param spec
	 *            HpfeedsActivationSpec
	 * @exception ResourceException
	 *                Thrown if an error occurs
	 */
	public HpfeedsActivation(HpfeedsRa ra, MessageEndpointFactory endpointFactory, HpfeedsActivationSpec spec) throws ResourceException	{
		this.ra = ra;
		this.endpointFactory = endpointFactory;
		this.spec = spec;
	}

	/**
	 * Get activation spec class
	 * 
	 * @return Activation spec
	 */
	public HpfeedsActivationSpec getActivationSpec() {
		return spec;
	}

	/**
	 * Get message endpoint factory
	 * 
	 * @return Message endpoint factory
	 */
	public MessageEndpointFactory getMessageEndpointFactory() {
		return endpointFactory;
	}

	/**
	 * Start the activation
	 * 
	 * @throws ResourceException
	 *             Thrown if an error occurs
	 */
	public void start() throws ResourceException {

	}

	/**
	 * Stop the activation
	 */
	public void stop() {

	}
	
}
