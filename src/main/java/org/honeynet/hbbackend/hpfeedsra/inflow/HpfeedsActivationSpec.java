package org.honeynet.hbbackend.hpfeedsra.inflow;

import javax.resource.spi.Activation;
import javax.resource.spi.ActivationSpec;
import javax.resource.spi.ConfigProperty;
import javax.resource.spi.InvalidPropertyException;
import javax.resource.spi.ResourceAdapter;

import org.honeynet.hpfeeds.Hpfeeds;


@Activation(messageListeners = { Hpfeeds.MessageHandler.class })
public class HpfeedsActivationSpec implements ActivationSpec {
	private ResourceAdapter ra;

	@ConfigProperty(defaultValue = "")
	private String channel;

	/**
	 * Default constructor
	 */
	public HpfeedsActivationSpec() {

	}

	/**
	 * Set channel
	 * 
	 * @param channel
	 *            The value
	 */
	public void setChannel(String channel) {
		this.channel = channel;
	}

	/**
	 * Get channel
	 * 
	 * @return The value
	 */
	public String getChannel() {
		return channel;
	}

	/**
	 * This method may be called by a deployment tool to validate the overall
	 * activation configuration information provided by the endpoint deployer.
	 * 
	 * @throws InvalidPropertyException
	 *             indicates invalid onfiguration property settings.
	 */
	public void validate() throws InvalidPropertyException {
	}

	/**
	 * Get the resource adapter
	 * 
	 * @return The handle
	 */
	public ResourceAdapter getResourceAdapter() {
		return ra;
	}

	/**
	 * Set the resource adapter
	 * 
	 * @param ra
	 *            The handle
	 */
	public void setResourceAdapter(ResourceAdapter ra) {
		this.ra = ra;
	}
	
}
