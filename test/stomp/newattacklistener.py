import time
import sys

# stomp.py 3.1.3
import stomp

class MyListener(object):
    def on_error(self, headers, message):
        print 'received error: %s' % message

    def on_message(self, headers, message):
        print 'received message:'
        print 'header:'
        print headers
        print 'message:'
        print message

conn = stomp.Connection(host_and_ports=[('localhost', 9972)], user='admin', passcode='admin')
conn.set_listener('', MyListener())
conn.start()
conn.connect()

conn.subscribe(destination='/topic/new_attack', ack='auto')

#conn.send('', destination='/topic/???')

try:
    time.sleep(60)
except KeyboardInterrupt:
    pass
finally:
    conn.disconnect()
