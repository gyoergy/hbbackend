import sys

# stomp.py 3.1.3
import stomp

def usage():
	print >> sys.stderr, 'usage: %s <ip>' % sys.argv[0]

if len(sys.argv) != 1+1:
	usage()
	exit(1)

ip = sys.argv[1]

conn = stomp.Connection(host_and_ports=[('localhost', 9972)], user='admin', passcode='admin')
conn.start()
conn.connect()

conn.send(message='', headers={'ip': ip}, destination='/topic/new_ip')
conn.disconnect()


# example usage:
# PGPASSWORD=hbbackend psql -h localhost -U hbbackend -d hbbackend -c 'select ip from ips_source;' -A  -F ' ' -t | xargs -I'{}' sh -c 'echo {} ; python test/stomp/send_to_new_ip.py {}'
#

