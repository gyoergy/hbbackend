import sys

# stomp.py 3.1.3
import stomp

def usage():
	print >> sys.stderr, 'usage: %s <id> <md5>' % sys.argv[0]

if len(sys.argv) != 2+1:
	usage()
	exit(1)

id = sys.argv[1]
md5 = sys.argv[2]

conn = stomp.Connection(host_and_ports=[('localhost', 9972)], user='admin', passcode='admin')
conn.start()
conn.connect()

conn.send(message='', headers={'id': id, 'md5': md5}, destination='/topic/new_binary')
conn.disconnect()


# example usage:
# PGPASSWORD=hbbackend psql -h localhost -U hbbackend -d hbbackend -c 'select id, md5 from binaries order by id;' -A  -F ' ' -t | xargs -I'{}' sh -c 'echo {} ; python test/stomp/send_to_new_binary.py {}'
#

